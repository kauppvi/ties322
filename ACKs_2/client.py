import socket
from modules import reliability

UDP_IP_ADDRESS = "127.0.0.1"
UDP_PORT_NO = 6789

sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

while True:
    message = input("> ")

    if message != "":

        # add reliability layer
        reliability.rdt_send(sock, message)

    else:
        print("?")