import random
import sys

class virtual_socket:
    def __init__(self, socket):
        self.socket = socket

    def recvfrom(self, buffsize):
        error_rand = random.randint(1,10)
        error_chance = 5 # out of 10

        # get bytes
        data, addr = self.socket.recvfrom(buffsize)

        # operate as integer
        int_data = int.from_bytes(data, byteorder=sys.byteorder)
        # generate random bit error
        if error_rand <= error_chance:
            rand = random.randint(1,int_data.bit_length())
            int_data ^= 1 << rand
            print("Error generated")
        else:
            print("No error generated")

        # return bytes
        bytes_data = int_data.to_bytes(len(data), byteorder=sys.byteorder)
        
        return addr, bytes_data