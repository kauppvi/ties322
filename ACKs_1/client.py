import socket
import crc8

UDP_IP_ADDRESS = "127.0.0.1"
UDP_PORT_NO = 6789

while True:
    message = input("> ")

    if message != "":
        message = message.encode()
        hash = crc8.crc8()
        hash.update(message)

        # combine message data and crc8 checksum
        package = message+hash.digest()

        sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        sock.sendto(package, (UDP_IP_ADDRESS, UDP_PORT_NO))
        print("Message sent")
    else:
        print("?")