import random
import time

class virtual_socket:
    def __init__(self, socket):
        self.socket = socket

    def recvfrom(self, buffsize):
        data, addr = self.socket.recvfrom(buffsize)
        time.sleep(random.randint(0,9))
        return data.decode()