import random
import sys

class virtual_socket:
    def __init__(self, socket):
        self.socket = socket

    def recvfrom(self, buffsize):
        error_rand = random.randint(1,10)
        error_chance = 8 # out of 10

        data, addr = self.socket.recvfrom(buffsize)
        int_data = int.from_bytes(data, byteorder=sys.byteorder)

        # generate random error
        if error_rand <= error_chance:
            rand = random.randint(1,int_data.bit_length())
            int_data ^= 1 << rand

        bytes_data = int_data.to_bytes(len(data), byteorder=sys.byteorder)
        return bytes_data.decode("utf-8", "replace")