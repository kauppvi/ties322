import socket
from virtual_socket import virtual_socket

UDP_IP_ADDRESS = "127.0.0.1"
UDP_PORT_NO = 6789

sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
sock.bind((UDP_IP_ADDRESS, UDP_PORT_NO))
virtual_sock = virtual_socket(sock)

print("Server started")

while True:
    message = virtual_sock.recvfrom(1024)
    print("Message:", message)