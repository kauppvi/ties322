import random

class virtual_socket:
    def __init__(self, socket):
        self.socket = socket

    def recvfrom(self, buffsize):
        p_drop = 5

        while True:
            data, addr = self.socket.recvfrom(buffsize)
            if random.randint(0,9) <= p_drop:
                print("Drop")
            else:
                return data.decode()