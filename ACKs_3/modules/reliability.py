import sys
import socket
from modules.virtual_socket import virtual_socket
from modules import crc8

UDP_IP_ADDRESS = "127.0.0.1"
UDP_PORT_NO = 6789

def rdt_send(sock, message):

    packet = make_pkt(message)
    udt_send(sock, packet)
    
    return

def make_pkt(message):
    # encode to bytes
    packet = message.encode()

    hash = crc8.crc8()
    hash.update(packet)

    # combine crc8 checksum and packet data
    digest = hash.digest()
    packet = digest+packet

    return packet

def udt_send(sock, packet):
    sock.sendto(packet, (UDP_IP_ADDRESS, UDP_PORT_NO))
    print("Packet sent, waiting for ACK/NACK...")

    ack, addr = sock.recvfrom(1024)
    ack = ack.decode()
    print ("Response: ", ack)

    while ack == "nack":
        udt_send(sock, packet)
    
    return

def rdt_rcv(sock):
    virtual_sock = virtual_socket(sock)

    # get packet and generate possible bit error
    addr, packet = virtual_sock.recvfrom(1024)
    print("Packet received: ", packet)

    # extract
    crc8_reminder, data_crc8, message = extract(packet)
    
    if crc8_reminder != data_crc8:
        sock.sendto("nack".encode(), addr)
    else:
        sock.sendto("ack".encode(), addr)
        deliver_data(message)

    return

def extract(packet):
    data_crc8 = packet[:1]
    data_message = packet[1:]

    hash = crc8.crc8()
    hash.update(data_message)
    crc8_reminder = hash.digest()

    return crc8_reminder, data_crc8, data_message

def deliver_data(message):
    print("Data delivered:", message)