import socket
from modules.virtual_socket import virtual_socket
from modules import reliability

UDP_IP_ADDRESS = "127.0.0.1"
UDP_PORT_NO = 6789

sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
sock.bind((UDP_IP_ADDRESS, UDP_PORT_NO))

print("Server started")

while True:
    
    # reliability layer process received and check for errors
    message = reliability.rdt_rcv(sock)